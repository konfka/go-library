package repositories

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	"gitlab.com/konfka/go-library/internal/entity"
	"log"
	"math/rand"
)

type DataBaseMethods interface {
	AddUser(user entity.User) (entity.User, error)
	ListUsers() []entity.UserResponse
	AddAuthor(author entity.Author) (entity.Author, error)
	ListAuthors() []entity.AuthorResponse
	TopAuthors(count int) []entity.AuthorTopResponse
	AddBook(book entity.Book) (entity.Book, error)
	ListBooks() []entity.BookResponse
	GetBook(userId int, bookId int) (entity.Book, error)
	ReturnBook(userId int, bookId int) (entity.Book, error)
}

type PostgresRepository struct {
	db *sqlx.DB
}

func NewPostgresRepository(db *sqlx.DB) *PostgresRepository {
	p := &PostgresRepository{db}
	p.PutDataToTables()
	return p
}

func (p PostgresRepository) PutDataToTables() {
	//Putting authors in table
	authors := p.ListAuthors()
	if authors == nil {
		for i := 0; i < 10; i++ {
			author := entity.Author{
				Name: gofakeit.Name(),
			}
			_, err := p.AddAuthor(author)
			if err != nil {
				log.Fatalln(err)
			}
		}
	}
	//Putting users in table
	users := p.ListUsers()
	if users == nil {
		for i := 0; i < 60; i++ {
			user := entity.User{
				Name: gofakeit.Name(),
			}
			_, err := p.AddUser(user)
			if err != nil {
				log.Fatalln(err)
			}
		}
	}
	//Putting books in table
	authors = p.ListAuthors()
	books := p.ListBooks()
	if books == nil {
		for i := 0; i < 100; i++ {
			randId := rand.Intn(len(authors))
			randTitleLen := rand.Intn(5) + 1
			book := entity.Book{
				Title:    gofakeit.Sentence(randTitleLen),
				AuthorId: randId,
			}
			_, err := p.AddBook(book)
			if err != nil {
				log.Fatalln(err)
			}
		}
	}
}

func (p PostgresRepository) AddUser(user entity.User) (entity.User, error) {
	var id int

	query := `INSERT INTO users (name) VALUES ($1) RETURNING id`

	err := p.db.QueryRow(query, user.Name).Scan(&id)
	if err != nil {
		return entity.User{}, err
	}

	user.Id = id
	return user, nil

}

func (p PostgresRepository) ListUsers() []entity.UserResponse {
	var res []entity.UserResponse

	query := `SELECT * FROM users`

	users, err := p.db.Queryx(query)
	if err != nil {
		log.Fatalln(err)
	}
	var ur entity.UserResponse
	var u entity.User
	for users.Next() {
		err = users.StructScan(&u)
		if err != nil {
			log.Fatalln(err)
		}

		var books []entity.Book

		q := `SELECT id, title, author_id FROM books WHERE user_id = $1`

		err = p.db.Select(&books, q, u.Id)
		if err != nil {
			log.Fatalln(err)
		}

		ur.RentedBooks, ur.User = books, u

		res = append(res, ur)
	}
	return res
}

func (p PostgresRepository) AddAuthor(author entity.Author) (entity.Author, error) {
	var id int

	query := `INSERT INTO authors (name) VALUES ($1) RETURNING id`

	err := p.db.QueryRow(query, author.Name).Scan(&id)
	if err != nil {
		return entity.Author{}, err
	}

	author.Id = id

	return author, nil
}

func (p PostgresRepository) ListAuthors() []entity.AuthorResponse {
	var res []entity.AuthorResponse

	query := `SELECT id, name FROM authors`

	authors, err := p.db.Queryx(query)
	if err != nil {
		log.Fatalln(err)
	}

	var a entity.Author
	var ar entity.AuthorResponse

	for authors.Next() {
		err = authors.StructScan(&a)
		if err != nil {
			log.Fatalln(err)
		}

		var books []entity.Book
		q := `SELECT id, title,author_id FROM books WHERE author_id=$1`

		err = p.db.Select(&books, q, a.Id)
		if err != nil {
			log.Fatalln(err)
		}

		ar.Author, ar.Books = a, books

		res = append(res, ar)
	}
	return res
}

func (p *PostgresRepository) TopAuthors(count int) []entity.AuthorTopResponse {
	var authors []entity.AuthorTopResponse

	query := `SELECT * FROM authors ORDER BY rating DESC LIMIT $1`

	err := p.db.Select(&authors, query, count)
	if err != nil {
		log.Fatalln(err)
	}

	return authors
}

func (p PostgresRepository) AddBook(book entity.Book) (entity.Book, error) {
	var id int

	query := `INSERT INTO books (title, author_id) VALUES ($1, $2) RETURNING id`

	err := p.db.QueryRow(query, book.Title, book.AuthorId).Scan(&id)
	if err != nil {
		return entity.Book{}, err
	}

	book.Id = id

	return book, nil
}

func (p PostgresRepository) ListBooks() []entity.BookResponse {
	var brArr []entity.BookResponse

	query := `SELECT id, title, author_id FROM books`

	books, err := p.db.Queryx(query)
	if err != nil {
		log.Fatalln(err)
	}

	var book entity.Book
	var br entity.BookResponse
	var author entity.Author

	for books.Next() {
		err = books.StructScan(&book)
		if err != nil {
			log.Fatalln(err)
		}

		q := `SELECT id, name FROM authors WHERE id=$1`

		err = p.db.Get(&author, q, book.AuthorId)
		if err != nil {
			log.Fatalln(err)
		}

		br.Book, br.Author = book, author
		brArr = append(brArr, br)
	}
	return brArr
}

func (p PostgresRepository) GetBook(userId int, bookId int) (entity.Book, error) {
	book, err := p.checkBookID(bookId)
	if err != nil {
		return entity.Book{}, err
	}

	_, err = p.checkUserID(userId)
	if err != nil {
		return entity.Book{}, err
	}

	id := p.checkBookStatus(bookId)

	if id.Valid {
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d is currently at user %d", bookId, id.Int64))
	}

	query := `UPDATE books SET user_id=$1 WHERE id=$2`
	p.db.MustExec(query, userId, bookId)

	query2 := `UPDATE authors SET rating=rating+1 WHERE id=$1`
	p.db.MustExec(query2, book.AuthorId)

	return book, nil
}

func (p PostgresRepository) ReturnBook(userId int, bookId int) (entity.Book, error) {
	book, err := p.checkBookID(bookId)
	if err != nil {
		return entity.Book{}, err
	}

	_, err = p.checkUserID(userId)
	if err != nil {
		return entity.Book{}, err
	}

	id := p.checkBookStatus(bookId)

	if !id.Valid {
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d is currently in library", bookId))
	}
	if id.Int64 != int64(userId) {
		return entity.Book{}, errors.New(fmt.Sprintf("user %d is not having book id: %d book", userId, bookId))
	}

	query := `UPDATE books SET user_id=null WHERE id=$1`
	p.db.MustExec(query, bookId)
	query2 := `UPDATE authors SET rating=rating-1 WHERE id=$1`
	p.db.MustExec(query2, book.AuthorId)

	return book, nil
}

func (p PostgresRepository) checkBookID(bookId int) (entity.Book, error) {
	query := `SELECT id, title, author_id FROM books WHERE id=$1`

	var book entity.Book
	err := p.db.Get(&book, query, bookId)
	if err != nil {
		log.Println(err)
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d error: %s", bookId, err.Error()))
	}

	return book, nil
}

func (p PostgresRepository) checkBookStatus(bookId int) sql.NullInt64 {
	query := `SELECT user_id FROM books WHERE id=$1`
	var id sql.NullInt64

	_ = p.db.Get(&id, query, bookId)

	return id
}

func (p PostgresRepository) checkUserID(userId int) (entity.User, error) {
	query := `SELECT name FROM users WHERE id=$1`

	var user entity.User
	err := p.db.Get(&user, query, userId)
	if err != nil {
		log.Println(err)
		return entity.User{}, errors.New(fmt.Sprintf("user id: %d error: %s", userId, err.Error()))
	}

	return user, nil
}
