package services

import "gitlab.com/konfka/go-library/internal/entity"

type DataBaseService interface {
	AddUser(user entity.User) (entity.User, error)
	ListUsers() []entity.UserResponse
	AddAuthor(author entity.Author) (entity.Author, error)
	ListAuthors() []entity.AuthorResponse
	TopAuthors(count int) []entity.AuthorTopResponse
	AddBook(book entity.Book) (entity.Book, error)
	ListBooks() []entity.BookResponse
	GetBook(userId int, bookId int) (entity.Book, error)
	ReturnBook(userId int, bookId int) (entity.Book, error)
}

type ServiceCentre struct {
	repo DataBaseService
}

func NewServiceCentre(repo DataBaseService) *ServiceCentre {
	return &ServiceCentre{repo: repo}
}

func (s ServiceCentre) AddUser(user entity.User) (entity.User, error) {
	return s.repo.AddUser(user)
}

func (s ServiceCentre) ListUsers() []entity.UserResponse {
	return s.repo.ListUsers()
}

func (s ServiceCentre) AddAuthor(author entity.Author) (entity.Author, error) {
	return s.repo.AddAuthor(author)
}

func (s ServiceCentre) ListAuthors() []entity.AuthorResponse {
	return s.repo.ListAuthors()
}

func (s ServiceCentre) TopAuthors(count int) []entity.AuthorTopResponse {
	return s.repo.TopAuthors(count)
}

func (s ServiceCentre) AddBook(book entity.Book) (entity.Book, error) {
	return s.repo.AddBook(book)
}

func (s ServiceCentre) ListBooks() []entity.BookResponse {
	return s.repo.ListBooks()
}

func (s ServiceCentre) GetBook(userId int, bookId int) (entity.Book, error) {
	return s.repo.GetBook(userId, bookId)
}

func (s ServiceCentre) ReturnBook(userId int, bookId int) (entity.Book, error) {
	return s.repo.ReturnBook(userId, bookId)
}
