package docs

import "gitlab.com/konfka/go-library/internal/entity"

//go:generate swagger generate spec -o ../../public/swagger.json --scan-models

// swagger:route POST /library/user/add user userAddRequest
// Добавление пользователя
//
// parameters:
// + name: UserName
// in: query
// description: User name
// required: true
//
// responses:
//	 200: userAddResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response userAddResponse
type userAddResponse struct {
	// in:body
	Body entity.User
}

// swagger:route Get /library/user/list user userListRequest
// Список пользователей с их книгами
//
// responses:
//	 200: userListResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response userListResponse
type userListResponse struct {
	// in:body
	Body []entity.UserResponse
}

// swagger:route POST /library/author/add author authorAddRequest
// Добавление автора
//
// parameters:
// + name: AuthorName
// in: query
// description: Author name
// required: true
//
// responses:
//	 200: authorAddResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response authorAddResponse
type authorAddResponse struct {
	// in:body
	Body entity.Author
}

// swagger:route Get /library/author/list author authorListRequest
// Список авторов
//
// responses:
//	 200: authorListResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response authorListResponse
type authorListResponse struct {
	// in:body
	Body []entity.AuthorResponse
}

// swagger:route Get /library/author/top author authorTopRequest
// Топ читаемых авторов
//
// parameters:
// + name: count
// in: query
// description: Count of authors
// required: true
//
// responses:
//	 200: authorTopResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response authorTopResponse
type authorTopResponse struct {
	// in:body
	Body []entity.AuthorTopResponse
}

// swagger:route POST /library/book/add book bookAddRequest
// Добавление автора
//
// parameters:
// + name: title
// in: query
// description: Title
// required: true
// + name: author_id
// in: query
// description: Author ID
// required: true
//
// responses:
//	 200: bookAddResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response bookAddResponse
type bookAddResponse struct {
	// in:body
	Body entity.Book
}

// swagger:route Get /library/book/list book bookListRequest
// Список авторов с их книгами
//
// responses:
//	 200: bookListResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response bookListResponse
type bookListResponse struct {
	// in:body
	Body []entity.UserResponse
}

// swagger:route POST /library/book/get book bookGetRequest
// Выдача книги пользователю
//
// parameters:
// + name: book_id
// in: query
// description: Book ID
// required: true
// + name: user_id
// in: query
// description: User ID
// required: true
//
// responses:
//	 200: bookGetResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response bookGetResponse
type bookGetResponse struct {
	// in:body
	Body entity.Book
}

// swagger:route POST /library/book/return book bookReturnRequest
// Возврат книги в библиотеку
//
// parameters:
// + name: book_id
// in: query
// description: Book ID
// required: true
// + name: user_id
// in: query
// description: User ID
// required: true
//
// responses:
//	 200: bookReturnResponse
//	 400: description: Bad request
//	 500: description: Internal server error

// swagger:response bookReturnResponse
type bookReturnResponse struct {
	// in:body
	Body entity.Book
}
