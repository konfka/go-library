package entity

type User struct {
	Id   int    `bd:"id"`
	Name string `bd:"name"`
}

type UserResponse struct {
	User        User
	RentedBooks []Book
}
