package entity

type Author struct {
	Id   int    `db:"id"`
	Name string `db:"name"`
}

type AuthorResponse struct {
	Author
	Books []Book
}

type AuthorTopResponse struct {
	Author
	Rating int `bd:"rating"`
}
