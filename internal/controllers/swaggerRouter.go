package controllers

import (
	"github.com/go-chi/chi"
	"gitlab.com/konfka/go-library/internal/template"
	"net/http"
)

func SwaggerRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", template.SwaggerUI)

	return r
}
