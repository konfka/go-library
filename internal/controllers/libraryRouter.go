package controllers

import (
	"github.com/go-chi/chi"
	"net/http"
)

func libraryRouter(c *Controller) http.Handler {
	r := chi.NewRouter()

	r.Post("/user/add", c.AddUser)
	r.Get("/user/list", c.ListUsers)
	r.Post("/author/add", c.AddAuthor)
	r.Get("/author/list", c.ListAuthors)
	r.Get("/author/top", c.TopAuthors)
	r.Post("/book/add", c.AddBook)
	r.Get("/book/list", c.ListBooks)
	r.Post("/book/get", c.GetBook)
	r.Post("/book/return", c.ReturnBook)

	return r
}
