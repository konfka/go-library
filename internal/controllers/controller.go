package controllers

import (
	"encoding/json"
	"gitlab.com/konfka/go-library/internal/entity"
	"net/http"
	"strconv"
)

type DataBaseContr interface {
	AddUser(user entity.User) (entity.User, error)
	ListUsers() []entity.UserResponse
	AddAuthor(author entity.Author) (entity.Author, error)
	ListAuthors() []entity.AuthorResponse
	TopAuthors(count int) []entity.AuthorTopResponse
	AddBook(book entity.Book) (entity.Book, error)
	ListBooks() []entity.BookResponse
	GetBook(userId int, bookId int) (entity.Book, error)
	ReturnBook(userId int, bookId int) (entity.Book, error)
}

type Controller struct {
	serv DataBaseContr
}

func NewController(serv DataBaseContr) *Controller {
	return &Controller{serv: serv}
}

func (c *Controller) AddUser(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	if username == "" {
		http.Error(w, "Empty user name", http.StatusBadRequest)
		return
	}

	user := entity.User{Name: username}

	user, err := c.serv.AddUser(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, user)

}

func (c *Controller) ListUsers(w http.ResponseWriter, r *http.Request) {
	list := c.serv.ListUsers()
	writeResponse(w, list)
}

func (c *Controller) AddAuthor(w http.ResponseWriter, r *http.Request) {
	auth := r.FormValue("author")
	if auth == "" {
		http.Error(w, "Empty author name", http.StatusBadRequest)
		return
	}

	ent := entity.Author{Name: auth}

	author, err := c.serv.AddAuthor(ent)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, author)
}

func (c *Controller) ListAuthors(w http.ResponseWriter, r *http.Request) {
	authors := c.serv.ListAuthors()
	writeResponse(w, authors)
}

func (c *Controller) TopAuthors(w http.ResponseWriter, r *http.Request) {
	count, err := strconv.Atoi(r.FormValue("count"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	authors := c.serv.TopAuthors(count)
	writeResponse(w, authors)
}

func (c *Controller) AddBook(w http.ResponseWriter, r *http.Request) {
	bookTitle := r.FormValue("title")
	if bookTitle == "" {
		http.Error(w, "Empty title", http.StatusBadRequest)
		return
	}

	bookAuthorId, err := strconv.Atoi(r.FormValue("author_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	book := entity.Book{AuthorId: bookAuthorId, Title: bookTitle}

	b, err := c.serv.AddBook(book)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, b)
}

func (c *Controller) ListBooks(w http.ResponseWriter, r *http.Request) {
	books := c.serv.ListBooks()
	writeResponse(w, books)
}

func (c *Controller) GetBook(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(r.FormValue("user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	bookId, err := strconv.Atoi(r.FormValue("book_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book, err := c.serv.GetBook(userId, bookId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, book)
}

func (c *Controller) ReturnBook(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(r.FormValue("user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	bookId, err := strconv.Atoi(r.FormValue("book_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book, err := c.serv.ReturnBook(userId, bookId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, book)
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(data)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
