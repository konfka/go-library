package api

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/konfka/go-library/internal/controllers"
	"gitlab.com/konfka/go-library/internal/repositories"
	"gitlab.com/konfka/go-library/internal/services"
)

func Run(config map[string]string, db *sqlx.DB) {
	r := repositories.NewPostgresRepository(db)
	s := services.NewServiceCentre(r)
	c := controllers.NewController(s)

	_ = c

	c.Start(config)
}
