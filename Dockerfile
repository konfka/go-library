FROM golang:alpine

WORKDIR /app

COPY . .

RUN go build -ldflags="-s -w" -o /service cmd/main.go

CMD ["/service"]

