package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/konfka/go-library/internal/api"
	"log"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	var config map[string]string
	config, err = godotenv.Read()
	if err != nil {
		log.Fatalln(err)
	}
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config["HOST"], config["DB_PORT"], config["DB_USER"], config["DB_PASSWORD"], config["DB_NAME"])
	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	api.Run(config, db)
}
