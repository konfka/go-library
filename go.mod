module gitlab.com/konfka/go-library

go 1.20

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/brianvoe/gofakeit/v6 v6.23.0 // indirect
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
